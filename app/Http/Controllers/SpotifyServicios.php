<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;

class SpotifyServicios
{

	function auntenticacion()
	{
		$client = new Client();

        $request = $client->request('POST', 'https://accounts.spotify.com/api/token', [
            'form_params' => [
                'grant_type' => 'client_credentials'
            ],
            'headers'  => [
                'Authorization' => 'Basic MmFiNzExNjA5MzFjNGFiYjgwZGRhNTk4M2Y1MTJlN2Y6YjIzYjkwZTFhMzY4NDlhMzgzOTRiODEwYThhZjg4NGE=',
                'Content-Type'  => 'application/x-www-form-urlencoded'
            ]   
        ]);

        return $request;
	}

	function getLanzamientos($token)
	{
        $client = new Client();

        $request = $client->request('GET', 'https://api.spotify.com/v1/browse/new-releases', [
            'headers' => [
                'Authorization' => 'Bearer '.$token,
            ]
        ]);

        $lanzamientos  = $request->getBody()->getContents();
        $aLanzamientos = json_decode($lanzamientos, true);
        $albunes       = $aLanzamientos["albums"];
        $items         = $albunes["items"];

        return $items;
	}

	function getArtista($idArtista, $token)
	{
		$client = new Client();

        $request = $client->request('GET', 'https://api.spotify.com/v1/artists/'.$idArtista, [
            'headers' => [
                'Authorization' => 'Bearer '.$token,
            ]  
        ]);

        $artista  = $request->getBody()->getContents();
        $aArtista = json_decode($artista, true);

        return $aArtista;
	}

	function getCancionesArtista($idArtista, $token, $pais)
	{
		$client = new Client();

        $request = $client->request('GET', 'https://api.spotify.com/v1/artists/'.$idArtista.'/top-tracks?country='.$pais, [
            'headers' => [
                'Authorization' => 'Bearer '.$token,
            ]  
        ]);

        $canciones  = $request->getBody()->getContents();
        $aCanciones = json_decode($canciones, true);
        $aCanciones = $aCanciones["tracks"];

        return $aCanciones;
	}
}