<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\SpotifyServicios;

class LanzamientosController extends Controller
{

    /**
     * Retorna la vista con los mas recientes lanzamientos.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        $spotifyServicios = new SpotifyServicios();
        
        $autorizacion   = $spotifyServicios->auntenticacion()->getBody()->getContents();


        $aAutorizacion  = json_decode($autorizacion, true);
        $token          = $aAutorizacion['access_token'];

        $items = $spotifyServicios->getLanzamientos($token);

        // dd($items);

        return view('content.home', ['items' => $items]);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
