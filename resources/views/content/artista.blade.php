
@extends('template')

@section('content')

		<div class="col-md-2">
			@foreach($artista["images"] as $id => $images)
				@php
					$img  = (object) $images;
				@endphp

				@if($id == 2)
					<img src="{{$img->url}}" alt="" class="rounded-circle circle-border max">
				@endif
			@endforeach
		</div>
		<div class="col-md-10 text-blanco pl-3">
			<h1>{{$artista["name"]}}</h1>
			@foreach($artista["external_urls"] as $item)
				<a href="{{ $item }}" target="_blank">Ir a la pagina del artista</a>
				@break
			@endforeach
		</div>
		<div class="offset-md-1 col-md-10 pt-5">
			<div class="table-responsive text-blanco">
				<table class="table">
					<thead>
						<tr>
							<th>Foto</th>
							<th>Album</th>
							<th>Canción</th>
						</tr>
					</thead>
					<tbody>
						@foreach($canciones as $cancion)
						<tr>
							<td>
								@foreach($cancion['album']['images'] as $id => $image)
									@if($id == 2)
										<img src="{{$image['url']}}" alt="" class="img-fluid">
										@break
									@endif 
								@endforeach
							</td>
							<td>
								{{$cancion['album']['name']}}
							</td>
							<td>{{$cancion['name']}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>	
		</div>

@endsection