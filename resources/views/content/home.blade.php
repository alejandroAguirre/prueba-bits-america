
@extends('template')

@section('content')

	@foreach($items as $index =>  $item)
		@php
			$itm = (object) $item;
		@endphp
		<div class="col-lg-4 col-md-6 pt-1 pb-1">
			@foreach($itm->artists as $artist)
				@break
			@endforeach
				<div class="card" style="width: 18rem;">
				  	@php
						$img  = (object) $itm->images;
					@endphp

					@foreach($img as $id => $im)
						@if($id == 1)
							<a href="{{ url('artista/'.$artist['id']) }}" class="btn-light enlace-sencillo">
				  				<img class="card-img-top" src="{{$im['url']}}" alt="Card image cap">
				  			</a>
				  		@endif
				  	@endforeach

				  	<div class="card-body">
				    	<h5 class="card-title">{{$itm->name}}</h5>
				    	
						@foreach($itm->artists as $artist)
							<a href="{{ url('artista/'.$artist['id']) }}" class="btn-light enlace-sencillo">
								<span class="badge badge-pill badge-primary">{{$artist["name"]}}</span>
							</a>
				    	@endforeach
				  	</div>
				</div>
		</div>
	@endforeach
@endsection